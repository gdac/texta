tool
class_name TextaPanel
extends MarginContainer


signal command_sent(command, data)

export(Color) var color := Color.lawngreen setget _set_color, _get_color
export(Vector2) var offset := Vector2(16, 16) setget _set_offset, _get_offset
export(String) var title := "untitled" setget _set_title, _get_title

var active := true

var _color := color
var _command_routes := {}
var _offset := offset
var _title := title

var __inbuilt_command_routes := {
	"activate": "activate",
	"deactivate": "deactivate",
}


func _draw():
	var _size := rect_size - _offset * 2
	draw_rect(Rect2(_offset, _size), _color, false)

	if has_node("LabelPivot/Label"):
		var label: Label = $LabelPivot/Label
		label.get_stylebox("normal").bg_color = _color

		# Workaround to ensure correct label size
		label.text = ""
		yield(get_tree().create_timer(0.0001), "timeout")
		label.rect_size.x = 0.0
		label.text = _title

# Set panel up for use
func activate():
	active = true

# Shut panel down to prevent use
func deactivate():
	active = false

# Determine a route for a given command
func get_route(command: String):
	if _command_routes.has(command):
		return _command_routes[command]

	if __inbuilt_command_routes.has(command):
		return __inbuilt_command_routes[command]

	# Use default route if exists
	if _command_routes.has("*"):
		return _command_routes["*"]

	return null

# Bubble up a command, formatting a single argument as an array of arguments
func send(target: String, command: String, data = null):
	if data != null:
		sendv(target, command, [data])
		return

	emit_signal("command_sent", target, command, null)

# Bubble up a command with data which is already an array of arguments
func sendv(target: String, command: String, data = null):
	emit_signal("command_sent", target, command, data)

# Called when a panel is set up
func setup():
	pass


# Internal functions

func _get_color() -> Color:
	return _color

func _set_color(c: Color):
	_color = c
	update()

func _get_offset() -> Vector2:
	return _offset

func _set_offset(o: Vector2):
	_offset = o
	update()

func _get_title() -> String:
	return _title

func _set_title(t: String):
	_title = t
	update()
