tool
class_name Optionlist
extends TextaPanel


onready var _options: VBoxContainer = $CenterContainer/Options


func _ready():
	_command_routes = {
		"show": "show_options",
	}

func clear_options():
	for option in _options.get_children():
		_options.remove_child(option)
		option.queue_free()

func show_options(options, target = "*"):
	clear_options()

	for option in options:
		var node: Button = Button.new()
		node.text = option.title
		node.connect("pressed", self, "_on_pressed", [option.id, target])
		node.owner = _options
		_options.add_child(node)

func _on_pressed(id: int, target: String):
	clear_options()
	send(target, "select_option", id)
