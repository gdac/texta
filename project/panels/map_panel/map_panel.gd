tool
extends TextaPanel


export(Texture) var map_texture: Texture setget _set_texture, _get_texture
export(Vector2) var map_offset: Vector2 setget _set_map_offset, _get_map_offset

onready var scroll_container: ScrollContainer = $ScrollContainer
onready var texture_rect: TextureRect = $ScrollContainer/TextureRect

var _map_offset: Vector2
var _map_texture: Texture


func _ready():
	_command_routes = {
		"set_image": "_receive_map_image",
		"set_position": "_receive_map_offset",
	}

	if _map_texture:
		texture_rect.texture = _map_texture


func _get_map_offset(	) -> Vector2:
	return _map_offset

func _get_texture() -> Texture:
	return _map_texture

func _receive_map_image(path: String):
	_set_texture(load(path))

func _receive_map_offset(data: String):
	if data.count(",") != 1:
		print("Malformed set_position command")
		return

	var split_data := data.split(",")
	scroll_container.scroll_horizontal = float(split_data[0])
	scroll_container.scroll_vertical = float(split_data[1])

func _set_map_offset(map_offset: Vector2):
	_map_offset = map_offset

	if scroll_container:
		scroll_container.scroll_horizontal = map_offset.x
		scroll_container.scroll_vertical = map_offset.y

func _set_texture(texture: Texture):
	_map_texture = texture

	if texture_rect:
		texture_rect.texture = texture
