tool
extends TextaPanel
class_name PictureFrame

export(Texture) var default_texture: Texture setget _set_default_texture, _get_default_texture

onready var texture_rect: TextureRect = $TextureRect

var _default_texture: Texture


func _ready():
	_command_routes = {
		"clear": "clear_picture",
		"set": "set_picture",
	}

	if _default_texture:
		texture_rect.texture = _default_texture

func clear_picture():
	texture_rect.texture = null

func set_picture(url):
	texture_rect.texture = load(url)

func _get_default_texture() -> Texture:
	return _default_texture

func _set_default_texture(texture: Texture):
	_default_texture = texture

	if texture_rect:
		texture_rect.texture = texture
