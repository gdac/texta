tool
extends TextaPanel


signal next_requested

export(String, FILE, "*.yarn") var yarn_file := ""
export(String) var initial_passage := "Start"
export(float) var scroll_speed := 66.6
export(NodePath) var options_list: NodePath

var prevent_indicator := false

var _last_message: Message

onready var _container: VBoxContainer = $ScrollContainer/VBoxContainer
onready var _indicator: Label = $Indicator
onready var _message_class = preload("messages/message.tscn")
onready var _yarn_player: YarnPlayer = $YarnPlayer


func _ready():
	_command_routes = {
		"select_option": "select_option",
	}

	_indicator.visible = false

func _process(delta: float):
	if not _last_message:
		return

	# Scroll the last message
	var last_value = _last_message.percent_visible
	var value = _last_message.percent_visible + delta * scroll_speed / _last_message.text.length()
	_last_message.percent_visible = min(1, value)

	# Show indicator when last message is finished scrolling
	if last_value < 1.0 and _last_message.percent_visible == 1.0 and not prevent_indicator:
		_indicator.visible = true

# Adds a message to the log
func add_message(text: String):
	prevent_indicator = false

	_last_message = _message_class.instance()
	_last_message.owner = _container
	_container.add_child(_last_message)

	_last_message.bbcode_text = text
	_last_message.percent_visible = 0.0

func select_option(option: int):
	_yarn_player.select_option(option)

func setup():
	_yarn_player.yarn.open(yarn_file)
	_yarn_player.play(initial_passage)

func _on_Clicker_pressed():
	_indicator.visible = false

	if not _last_message:
		return

	if _last_message.percent_visible < 1.0:
		_last_message.percent_visible = 1.0

		if not prevent_indicator:
			_indicator.visible = true
		return

	_yarn_player.do_next()

func _on_YarnPlayer_command_triggered(target, arguments):
	if target == "wait":
		return

	sendv(target, arguments[0], Array(arguments).slice(1, arguments.size() - 1))
	_yarn_player.do_next()

func _on_YarnPlayer_dialogue_triggered(speaker, text):
	add_message(text)

func _on_YarnPlayer_options_shown(titles):
	if not options_list:
		return

	var target = options_list.get_name(options_list.get_name_count() - 1)
	sendv(target, "show", [titles, name])
