extends Control
class_name UI


onready var panel_layout: Control = $PanelLayout

var _panels := []


# Called when the node enters the scene tree for the first time.
func _ready():
	_add_panels(panel_layout.get_children())

func _add_panels(children):
	for child in children:
		if child is TextaPanel:
			child.connect("command_sent", self, "_send_command")
			child.setup()
			_panels.append(child)
			continue

		_add_panels(child.get_children())

func _send_command(target: String, command: String, data):
	for panel in _panels:
		if target != "*" and target != panel.name:
			continue

		var route = panel.get_route(command)
		if route:
			panel.callv(route, data)
